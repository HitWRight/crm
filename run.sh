#!/bin/sh

SCRIPT=$(readlink -f "$0")
BASEDIR=$(dirname "$SCRIPT")
USER="odoo"
DB="odoo"
PASS="X9odoopasw"

if [[ "$VIRTUAL_ENV" != "" ]]
then
	echo "Succ"
else
	echo "You're not in python virtual environment!"
	exit 1
fi

echo "$BASEDIR"

"$BASEDIR/odoo/odoo-bin" -u $USER -d $DB -w $PASS --addons-path=$BASEDIR/odoo/addons,$BASEDIR/odoo_addons --update=aurora_addon

